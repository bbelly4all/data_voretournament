#!/usr/bin/env bash
qcc="../../../../gmqcc/gmqcc"
qcsrc="../data/xonotic-data.pk3dir/qcsrc"
mod="../../../data_voretournament"

os=$(uname -o)
arch=$(uname -m)

if [ "${os}" == "Msys" ] || [ "${os}" == "Cygwin" ] ; then
	make -C ${qcsrc} QCC=${qcc}.exe BUILD_MOD=${mod}
else
	make -C ${qcsrc} QCC=${qcc} BUILD_MOD=${mod}
fi
