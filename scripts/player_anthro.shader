textures/player/anthro_body_lupine
{
	{
		map textures/player/anthro_body_lupine.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_body_vulpine
{
	{
		map textures/player/anthro_body_vulpine.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_body_felidae
{
	{
		map textures/player/anthro_body_felidae.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_body_sciuridae
{
	{
		map textures/player/anthro_body_sciuridae.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_body_lapine
{
	{
		map textures/player/anthro_body_lapine.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_mouth
{
	dpreflectcube cubemaps/default/sky
	{
		map textures/player/anthro_mouth.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_eye
{
	dpreflectcube cubemaps/default/sky
	{
		map textures/player/anthro_eye.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_hair
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 10 sin 0 0.1 0 1
	{
		map textures/player/anthro_hair.tga
		rgbgen lightingDiffuse
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/player/anthro_internal_mouth
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 50 sin -0.1 0.2 0 0.5
	{
		map textures/player/anthro_mouth.tga
		rgbgen lightingDiffuse
	}
}

textures/player/anthro_internal_stomach
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 100 sin -0.5 1.0 0 0.5
	{
		map textures/player/anthro_stomach.tga
		rgbgen lightingDiffuse
	}
}
